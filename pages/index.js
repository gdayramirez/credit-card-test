import Header from '../components/Header';
import Creditcard from '../components/Creditcard';
import 'semantic-ui-css/semantic.min.css';

function App() {
  return (
    <div className='App'>
      <Header />
      <Creditcard />
    </div>
  );
}

export default App;